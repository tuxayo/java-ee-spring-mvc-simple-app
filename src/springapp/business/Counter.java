package springapp.business;

public class Counter {
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int counter) {
        this.count = counter;
    }
    
    public void increment() {
        count++;
    }
}
