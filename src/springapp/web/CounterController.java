package springapp.web;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import springapp.business.Counter;

@Controller()
public class CounterController {
    protected final Log logger = LogFactory.getLog(getClass());

    /**
     * example url:
     * http://localhost:8080/java-ee-spring-mvc-simple-app/actions/counter/foobar 
     */
    @RequestMapping(value = "/counter/{message}", method = RequestMethod.GET)
    public ModelAndView countAndDisplay(
            HttpServletRequest request,
            @PathVariable(value = "message") String message
            ) {
        logger.info("Running " + this);

        // Business stuff (should be extracted in a service)
        ServletContext context = request.getServletContext();
        Counter counter = (Counter) context.getAttribute("counter");
        if(counter == null) {
            counter  = new Counter();
        }
        counter.increment();
        context.setAttribute("counter", counter);

        // Controller stuff
        ModelAndView modelAndView = new ModelAndView("counter")
                .addObject("message", message)
                .addObject("count", counter.getCount());
        return modelAndView;
    }
}
