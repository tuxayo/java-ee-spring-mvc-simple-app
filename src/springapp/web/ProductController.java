package springapp.web;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import springapp.business.ProductManager;
import springapp.model.Product;

@Controller()
@RequestMapping("/product")
public class ProductController {

    @Autowired
    ProductManager manager;

    @Autowired
    ProductValidator validator;

    protected final Log logger = LogFactory.getLog(getClass());

//    @RequestMapping(value = "/list", method = RequestMethod.GET)
//    public ModelAndView listProducts() {
//        logger.info("List of products");
//        Collection<Product> products = manager.findAll();
//        return new ModelAndView("productsList", "products", products);
//    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String listProducts2() {
        logger.info("List of products");
        return "productsList";
    }

    @ModelAttribute("products")
    Collection<Product> products() {
        logger.info("Making list of products");
        return manager.findAll();
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editProduct(@ModelAttribute Product p) {
        return "productForm";
    }
    
    @ModelAttribute
    public Product newProduct(
            @RequestParam(value = "id", required = false) Integer productNumber) {
        if (productNumber != null) {
            logger.info("find product " + productNumber);
            return manager.find(productNumber);
        }
        Product p = new Product();
        p.setNumber(null);
        p.setName("");
        p.setPrice(0.0);
        p.setDescription("");
        logger.info("new product = " + p);
        return p;
    } 
    
    @ModelAttribute("productTypes")
    public Map<String, String> productTypes() {
        Map<String, String> types = new LinkedHashMap<>();
        types.put("type1", "Type 1");
        types.put("type2", "Type 2");
        types.put("type3", "Type 3");
        types.put("type4", "Type 4");
        types.put("type5", "Type 5");
        return types;
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute Product p, BindingResult result) {
        // If one let the price field empty when editing, the validator will
        // NullPointException and the product will get saved
        // anyway...
        // Because we are saving in memory and we are using a reference to the
        // final object, so it's already saved at any moment. What a trap.
        validator.validate(p, result);
        if (result.hasErrors()) {
            return "productForm";
        }
        manager.save(p);
        return "redirect:list";
    }

}
