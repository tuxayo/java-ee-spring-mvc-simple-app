package springapp.web;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

public class HelloControllerTest {

    @Test
    public void testHandleRequest() throws ServletException, IOException {
        HelloController helloController = new HelloController();
        ModelAndView modelAndView = helloController.handleRequest(null, null);
        String message = (String) modelAndView.getModelMap().get("message");
        assertEquals("this is a message", message);
    }
}
