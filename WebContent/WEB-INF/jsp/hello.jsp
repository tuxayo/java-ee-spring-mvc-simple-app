<%@ include file="/WEB-INF/jsp/include.jsp" %>

<html>
  <head><title>Hello :: Spring Application</title></head>
  <body>
    <h1>Hello - Spring Application</h1>
    <p>Greetings, it is now <c:out value="${now}" default="None" /></p>
    <p>Here is a message: <c:out value="${message}" default="None" /></p>

	<c:url var="productList" value="/actions/product/list" />
	<p><a href="${productList}">${productList}</a></p>

	<c:url var="welcome" value="/actions/tests/welcome" />
	<p><a href="${welcome}">${welcome}</a></p>

	<c:url var="root" value="/" />
	<p><a href="${root}">${root}</a></p>

	<c:url var="counter" value="/actions/counter/foobar" />
	<p><a href="${counter}">${counter}</a></p>

	<c:url var="hello" value="/hello.htm" />
	<p><a href="${hello}">${hello}</a></p>

    <p>Language : <a href="?language=en">English</a>|<a href="?language=fr_FR">French</a></p>
    <p>Current Locale : ${pageContext.response.locale}</p>

  </body>
</html>
