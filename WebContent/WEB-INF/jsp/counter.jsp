<%@ include file="/WEB-INF/jsp/include.jsp" %>

<html>
  <head><title>Counter :: Spring Application</title></head>
  <body>
    <h1>Counter</h1>
    <p>Here is a message from the request params: <c:out value="${message}" default="None" /></p>
    <p>Count: <c:out value="${count}" default="None" /></p>

</body>
</html>
